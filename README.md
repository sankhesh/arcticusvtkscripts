# Arcticus VTK Scripts

Welcome to Arcticus VTK scripts. This repository provides the scripts necessary to set up
the environment, compile dependencies and vtk for Xe OSPRay rendering of VTK-m datasets.
