#!/usr/bin/env zsh

# modules
source /etc/profile.d/modules.sh

# Find all the module files
module use /soft/restricted/CNDA/modulefiles
module use --append /soft/modulefiles

# oneapi
module load oneapi/release/2022.10.15.003

# meson
module load meson

# ninja
module load ninja

# cmake
module load cmake/3.23.2

# icx/dpcpp
export CC=`which icx`
export ICXX=`which icpx`
export CXX=`which dpcpp`

# Flags
export CFLAGS="-g -O2 -fPIC -pthread -qopenmp -fopenmp-targets=spir64"
export CXXFLAGS="-g -O2 -fPIC -pthread -Wno-openmp-mapping -fiopenmp -D__STRICT_ANSI__ -Wno-tautological-constant-compare -qopenmp -fopenmp-targets=spir64"
export LFLAGS="/soft/restricted/CNDA/sdk/2022.06.30.001/oneapi/compiler/pseudo-20220630/compiler/linux/lib/libpi_level_zero.so -pthread -qopenmp -fopenmp-targets=spir64"
