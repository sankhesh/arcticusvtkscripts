#!/usr/bin/env zsh

CWD=$(pwd)

# env
MYDIR=${0:a:h}
source $MYDIR/env.zsh

DEPS_DIR=$HOME/deps
DEPS_PREFIX=$DEPS_DIR/prefix
BLD_DIR=$DEPS_DIR/kokkos-bld
if [ ! -d "$DEPS_DIR" ]
then
  mkdir -p $DEPS_DIR
fi

cd $DEPS_DIR

git clone https://github.com/kokkos/kokkos.git
cd kokkos

mkdir -p $BLD_DIR
cd $BLD_DIR

# Configure
cmake\
  -GNinja\
  -DCMAKE_C_COMPILER=$CC\
  -DCMAKE_CXX_COMPILER=$ICXX\
  -DCMAKE_CXX_STANDARD=17\
  -DCMAKE_BUILD_TYPE=RelWithDebInfo\
  -DCMAKE_C_FLAGS="$CFLAGS"\
  -DCMAKE_CXX_FLAGS="$CXXFLAGS"\
  -DCMAKE_EXE_LINKER_FLAGS="$LFLAGS"\
  -DCMAKE_INSTALL_PREFIX=$DEPS_PREFIX\
  -DKokkos_ENABLE_OPENMP=ON\
  -DKokkos_ENABLE_OPENMPTARGET=ON\
  -DKokkos_ARCH_INTEL_PVC=ON\
  $DEPS_DIR/kokkos

# Build
ninja

# Install
ninja install

cd $CWD
