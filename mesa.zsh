#!/usr/bin/env zsh

CWD=$(pwd)

# env
MYDIR=${0:a:h}
source $MYDIR/env.zsh

DEPS_DIR=$HOME/deps
DEPS_PREFIX=$DEPS_DIR/prefix
BLD_DIR=$DEPS_DIR/mesa-bld
if [ ! -d "$DEPS_DIR" ]
then
  mkdir -p $DEPS_DIR
fi

cd $DEPS_DIR

export CC=`which gcc`
export CXX=`which g++`
export CFLAGS=""
export CXXFLAGS=""
export LFLAGS=""

git clone https://gitlab.freedesktop.org/mesa/mesa.git
cd mesa
git checkout mesa-20.0.0

mkdir -p $BLD_DIR
meson $BLD_DIR\
  -Dplatforms=''\
  -Dglx=disabled\
  -Dosmesa=gallium\
  -Dgallium-drivers=swrast\
  -Dvulkan-drivers=''\
  -Dprefix=$DEPS_PREFIX

ninja -C $BLD_DIR install

cd $CWD
