#!/usr/bin/env zsh

CWD=$(pwd)

# env
MYDIR=${0:a:h}
source $MYDIR/env.zsh

# OSPRay source info
OSPRAY_FORK=demarle
#OSPRAY_SHA=v2.9.0
OSPRAY_SHA=demonstrate_level0_memtype

DEPS_DIR=$HOME/deps
DEPS_PREFIX=$DEPS_DIR/prefix
BLD_DIR=$DEPS_DIR/ospray-bld
if [ ! -d "$DEPS_DIR" ]
then
  mkdir -p $DEPS_DIR
fi

cd $DEPS_DIR

git clone https://github.com/$OSPRAY_FORK/ospray
cd ospray
git checkout $OSPRAY_SHA

mkdir -p $BLD_DIR
cd $BLD_DIR

#Configure
cmake\
  -GNinja\
  -DCMAKE_C_COMPILER=$CC\
  -DCMAKE_CXX_COMPILER=$CXX\
  -DCMAKE_BUILD_TYPE=RelWithDebInfo\
  -DCMAKE_C_FLAGS="$CFLAGS"\
  -DCMAKE_CXX_FLAGS="$CXXFLAGS"\
  -DCMAKE_EXE_LINKER_FLAGS="$LFLAGS"\
  -DCMAKE_INSTALL_PREFIX=$DEPS_PREFIX\
  -DBUILD_GLFW=OFF\
  -DBUILD_OSPRAY_APPS=OFF\
  -DBUILD_EMBREE_FROM_SOURCE=OFF\
  -DINSTALL_IN_SEPARATE_DIRECTORIES=OFF\
  -DDEPENDENCIES_BUILD_TYPE=RelWithDebInfo\
  -DBUILD_JOBS:STRING=12\
  $DEPS_DIR/ospray/scripts/superbuild

# Build
ninja

# Install
cd $BLD_DIR/ospray/build
ninja install

cd $CWD
